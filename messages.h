#pragma once

#include <array>
#include <string_view>

constexpr std::array<const std::string_view, 5> messages{
    "how are you",         "good, how are you",
    "ok I guess, and you", "yeah, chill, things are going",
    "same same",
};
