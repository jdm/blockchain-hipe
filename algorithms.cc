#include "algorithms.h"

#include <cassert>

#include "hash.h"

namespace tma {
    std::string calculate_nonce(std::string const& x, int num_zeros) {
    // Exclude stuff that does not work nicely in a string.
    constexpr U64 min_ascii{33};
    constexpr U64 max_ascii{126};

    const U64 factor = std::pow(10, num_zeros);
    const U64 h = hash_string(x);

    U64 diff = diff_to_next(h, factor);
    assert(diff <= 100);

    char c;
    if (diff >= min_ascii) {
        c = i2ascii(diff);
    } else if (diff <= (max_ascii - 100)) {
        c = i2ascii(100 + diff);
    } else {  // 26 < diff < 33
        c = i2ascii(100 + (diff - min_ascii));
    }
    return std::string{} + c;
}
}
