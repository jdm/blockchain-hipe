#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
// #include <functional> // does this contain std::hash?

#include "block.h"
#include "messages.h"
#include "typedefs.h"

namespace tma {
struct Chain {
    std::vector<Block> blocks;

    static Chain init() { return Chain{{Block::init()}}; }

    void add_message(std::string const& msg) {
        blocks.push_back(Block::from(blocks.back(), msg));
    }

    auto begin() { return blocks.begin(); }

    auto end() { return blocks.end(); }
};
}  // namespace tma

int main() {
    auto c = tma::Chain::init();
    for (auto s : messages) {
        c.add_message(std::string{s});
    }
    for (auto b : c) {
        std::cout << "--- block ---\n";
        std::cout << b << "\n";
    }
}
