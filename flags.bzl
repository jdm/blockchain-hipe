""" Global variables used by different packages. """

CC_FLAGS = [
    "-std=c++2b",
    "-Wall",
    "-Wpedantic",
    "-Wunused-value",
]
