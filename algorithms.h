#pragma once

#include <string>

#include "typedefs.h"

namespace tma {

inline U64 i2ascii(U64 x) { return static_cast<char>(x); }

inline U64 diff_to_next(U64 x, U64 next) { return next - (x - (x / next) * next); }

std::string calculate_nonce(std::string const& x, int num_zeros = 2);
}  // namespace tma
