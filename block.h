#pragma once

#include "algorithms.h"
#include "hash.h"
#include "typedefs.h"

namespace tma {

struct Block {
    Hash prev_hash;
    std::string nonce;
    std::string content;

    static Block init() { return {0, calculate_nonce("0"), ""}; }

    static Block from(Block const& other, std::string const& msg) {
        U64 h = other.to_hash();
        return Block{
            h,
            calculate_nonce(msg + std::to_string(h)),
            msg,
        };
    }

    std::string to_string() const {
        return std::to_string(prev_hash) + nonce + content;
    }

    Hash to_hash() const { return hash_string(to_string()); }
};
}  // namespace tma

std::ostream& operator<<(std::ostream& os, tma::Block const& b) {
    os << "msg: " << b.content << "\n";
    os << "prev hash: " << b.prev_hash << "\n";
    return os;
}
