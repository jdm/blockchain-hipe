#pragma once

#include <cmath>
#include <string>

#include "typedefs.h"

namespace tma {
inline Hash hash_string(std::string const& x) {
    Hash h{0};
    for (char const& c : x) h += static_cast<Hash>(c);
    return h;
}
}  // namespace tma
